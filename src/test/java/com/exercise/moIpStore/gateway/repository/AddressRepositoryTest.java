package com.exercise.moIpStore.gateway.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

import com.exercise.moIpStore.domains.Address;
import com.exercise.moIpStore.templates.AddressTemplates;
import com.exercise.moIpStore.templates.ZipCodeTemplates;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddressRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private AddressRepository repository;

	private Address entity;
	
    @BeforeClass
    public static void setupClass() {
        FixtureFactoryLoader.loadTemplates("com.exercise.moIpStore.templates");
    }
    
	@Before
	public void setUp() {
		entity = Fixture.from(Address.class).gimme(AddressTemplates.ADDRESS1);
		repository.save(entity);
	}

	@Test
	public void testFindByZipCode() {
		final List<Address> addresses = repository.findByZipCode(ZipCodeTemplates._09895000);
		assertThat(addresses).isNotEmpty();
		assertThat(addresses.size()).isEqualTo(1);
		
		final Address address = addresses.get(0);
		assertThat(address.getId()).isNotNull();
		assertThat(address.getZip()).isEqualTo(entity.getZip());
		assertThat(address.getNumber()).isEqualTo(entity.getNumber());
		assertThat(address.getStreet()).isEqualTo(entity.getStreet());
		assertThat(address.getDistrict()).isEqualTo(entity.getDistrict());
		assertThat(address.getAddressLine2()).isEqualTo(entity.getAddressLine2());
		assertThat(address.getZip().getCity()).isEqualTo(entity.getZip().getCity());
		assertThat(address.getZip().getCountry()).isEqualTo(entity.getZip().getCountry());
		assertThat(address.getZip().getStateOrProvince()).isEqualTo(entity.getZip().getStateOrProvince());
	}
	
	@Test
	public void testFindByInvalidZipCode() {
		final List<Address> address = this.repository.findByZipCode("00000000");
		assertThat(address).isEmpty();
	}

}

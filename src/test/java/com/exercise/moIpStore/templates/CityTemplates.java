package com.exercise.moIpStore.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import com.exercise.moIpStore.domains.City;

public class CityTemplates implements TemplateLoader {

	public static final String SAO_BERNARDO = "SAO_BERNARDO";

	@Override
	public void load() {
        Fixture.of(City.class).addTemplate(SAO_BERNARDO, new Rule() {{
            add("name", "São Bernardo");
        }});
	}

}

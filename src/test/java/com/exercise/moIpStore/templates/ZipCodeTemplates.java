package com.exercise.moIpStore.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import com.exercise.moIpStore.domains.City;
import com.exercise.moIpStore.domains.Country;
import com.exercise.moIpStore.domains.StateOrProvince;
import com.exercise.moIpStore.domains.ZipCode;

public class ZipCodeTemplates implements TemplateLoader {

	public static final String _09895000 = "09895000";

	@Override
	public void load() {
        Fixture.of(ZipCode.class).addTemplate(_09895000, new Rule() {{
            add("code", "09895000");
            add("city", one(City.class, CityTemplates.SAO_BERNARDO));
            add("stateOrProvince", one(StateOrProvince.class, StateOrProvinceTemplates.SAO_PAULO));
            add("country", one(Country.class, CountryTemplates.BRASIL));
        }});
	}

}

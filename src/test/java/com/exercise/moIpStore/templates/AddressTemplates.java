package com.exercise.moIpStore.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import com.exercise.moIpStore.domains.Address;
import com.exercise.moIpStore.domains.AddressType;
import com.exercise.moIpStore.domains.StreetSuffix;
import com.exercise.moIpStore.domains.ZipCode;

public class AddressTemplates implements TemplateLoader {

	public static final String ADDRESS1 = "Address1";

	@Override
	public void load() {
        Fixture.of(Address.class).addTemplate(ADDRESS1, new Rule() {{
            add("zip", one(ZipCode.class, ZipCodeTemplates._09895000));
            add("type", AddressType.HOME);
            add("street", "Prestes Maia");
            add("number", "123");
            add("district", "Nova Petropolis");
            add("addressLine2", "apto 1");
            add("streetSuffix", StreetSuffix.AVENUE);
        }});

	}

}

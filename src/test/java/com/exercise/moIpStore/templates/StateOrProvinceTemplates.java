package com.exercise.moIpStore.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import com.exercise.moIpStore.domains.StateOrProvince;

public class StateOrProvinceTemplates implements TemplateLoader {

	public static final String SAO_PAULO = "SAO PAULO";

	@Override
	public void load() {
        Fixture.of(StateOrProvince.class).addTemplate(SAO_PAULO, new Rule() {{
            add("name", "São Paulo");
            add("acronym", "SP");
        }});
	}

}

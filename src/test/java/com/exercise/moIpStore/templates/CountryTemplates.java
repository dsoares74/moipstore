package com.exercise.moIpStore.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import com.exercise.moIpStore.domains.Country;

public class CountryTemplates implements TemplateLoader {

	public static final String BRASIL = "BRASIL";

	@Override
	public void load() {
        Fixture.of(Country.class).addTemplate(BRASIL, new Rule() {{
            add("code", "BR");
            add("name", "Brasil");
        }});
	}

}

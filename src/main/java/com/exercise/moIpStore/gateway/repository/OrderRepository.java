package com.exercise.moIpStore.gateway.repository;

import org.springframework.data.repository.CrudRepository;

import com.exercise.moIpStore.domains.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

}

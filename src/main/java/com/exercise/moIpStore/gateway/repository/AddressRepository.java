package com.exercise.moIpStore.gateway.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.exercise.moIpStore.domains.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {
	
	List<Address> findByZipCode(String zipCode);
	
}

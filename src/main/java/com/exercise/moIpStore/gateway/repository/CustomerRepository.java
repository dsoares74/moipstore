package com.exercise.moIpStore.gateway.repository;

import org.springframework.data.repository.CrudRepository;

import com.exercise.moIpStore.domains.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}

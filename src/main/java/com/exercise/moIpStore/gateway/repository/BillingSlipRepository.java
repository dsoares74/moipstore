package com.exercise.moIpStore.gateway.repository;

import org.springframework.data.repository.CrudRepository;

import com.exercise.moIpStore.domains.BillingSlip;

public interface BillingSlipRepository extends
		CrudRepository<BillingSlip, Long> {

}

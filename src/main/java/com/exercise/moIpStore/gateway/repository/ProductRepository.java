package com.exercise.moIpStore.gateway.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import com.exercise.moIpStore.domains.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	Collection<Product> findByBrandName(String brand);
	
	Product findByCode(String code);
	
	Collection<Product> findByCategoriesCode(String category);
}

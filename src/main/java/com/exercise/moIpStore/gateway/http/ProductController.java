package com.exercise.moIpStore.gateway.http;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.moIpStore.domains.Product;
import com.exercise.moIpStore.usecases.FindProduct;

@RestController
@RequestMapping(value = "/product")
public class ProductController {
	
	private final FindProduct findProduct;
	
	@Autowired
	public ProductController(final FindProduct findProduct) {
		this.findProduct = findProduct;
	}

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/brand", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<Product> findByBrand(@RequestParam("name") String name) {
		return findProduct.findByBrand(name);
	}
}

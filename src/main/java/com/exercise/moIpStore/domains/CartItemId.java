package com.exercise.moIpStore.domains;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Embeddable
@EqualsAndHashCode
public class CartItemId implements Serializable {

	private static final long serialVersionUID = -2770316085602436480L;

	@ManyToOne
	private Product product;
	
	@ManyToOne
	private Cart cart;
	
}

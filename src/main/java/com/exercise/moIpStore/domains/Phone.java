package com.exercise.moIpStore.domains;

import static javax.persistence.EnumType.STRING;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_phone")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class Phone extends BaseEntity {

	private static final long serialVersionUID = -5344724503253973786L;

	@Id
	@GenericGenerator(
	        name = "IdPhoneGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "phone_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdPhoneGen")
	private Long id;

	@Enumerated(STRING)
	@NotNull(message = "{phone.type.notnull}")
	private PhoneType type;

	@NotNull(message = "{phone.number.notnull}")
	private String number;

	private boolean main = false;
}

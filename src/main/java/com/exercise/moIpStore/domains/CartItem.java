package com.exercise.moIpStore.domains;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_cart_item")
@EqualsAndHashCode(of = { "id" })
@AssociationOverrides({
		@AssociationOverride(name = "id.cart",
			joinColumns = @JoinColumn(name = "cart_id")),
		@AssociationOverride(name = "id.product",
			joinColumns = @JoinColumn(name = "product_id")) })
public class CartItem implements Serializable {

	private static final long serialVersionUID = 5249701102211205011L;

	@EmbeddedId
	private CartItemId id;
	
	@Column(name = "quantity", nullable = false)
	private Integer quantity;

}

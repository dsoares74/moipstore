package com.exercise.moIpStore.domains;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.exercise.moIpStore.crypt.CryptoConverter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_credit_card")
@EqualsAndHashCode(of = { "number" })
public class CreditCard implements Serializable {

	private static final long serialVersionUID = 4973195938211036148L;

	@Id
	@GenericGenerator(
	        name = "IdCreditCardGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "creditcard_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdCreditCardGen")
	private Long id;

	@Convert(converter = CryptoConverter.class)
	@Column(name = "number", unique = true, nullable = false, length = 16)
	private String number;

	private String cardHolder;

	private String cardBrand;

	private String bankCode;

	private String securityCode;

	private String expirationMonth;

	private String expirationYear;
}

package com.exercise.moIpStore.domains;

public enum PhoneType {
	HOME, WORK, WORK_FAX, HOME_FAX, MOBILE;
}

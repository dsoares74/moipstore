package com.exercise.moIpStore.domains;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_customer")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class Customer extends BaseEntity {

	private static final long serialVersionUID = -7755145684199594322L;

	@Id
	@GenericGenerator(
	        name = "IdCustomerGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "customer_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdCustomerGen")
	private Long id;

	@Column(name = "first_name", nullable = false, length = 16)
	private String firstName;

	@Column(name = "middle_name", length = 16)
	private String middleName;

	@Column(name = "last_name", nullable = false, length = 16)
	private String lastName;

	@Column(name = "birth_date")
	private LocalDate birthDate;

	private String email;

	@OneToMany(cascade = ALL, fetch = EAGER, orphanRemoval = true)
	@JoinColumn(name = "id_customer")
	private Collection<Address> addresses = new HashSet<>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = ALL, orphanRemoval = true)
	@JoinColumn(name = "id_customer")
	private Collection<Phone> phones = new HashSet<>();

	public Optional<Address> findAddressByType(final AddressType type) {
		return addresses.stream().filter(a -> a.getType() == type).findFirst();
	}

	public Optional<Phone> findPhoneByType(final PhoneType type) {
		return phones.stream().filter(p -> p.getType() == type).findFirst();
	}

	public Optional<Phone> findMainPhone() {
		return phones.stream().filter(Phone::isMain).findFirst();
	}
}

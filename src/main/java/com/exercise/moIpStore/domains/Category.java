package com.exercise.moIpStore.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_category")
@EqualsAndHashCode(of = { "code" }, callSuper = false)
public class Category extends BaseEntity {

	private static final long serialVersionUID = 1896516665822640766L;
	
	@Id
	private Long code;

	private String name;

	private boolean active;

	private Category parent;

}

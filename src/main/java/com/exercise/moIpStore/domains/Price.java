package com.exercise.moIpStore.domains;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Embeddable
@EqualsAndHashCode
public class Price implements Serializable {

	private static final long serialVersionUID = -1815511115290556332L;

	@Column(name = "list_price")
	private Long listPriceInCents;

	@Column(name = "sale_price")
	private Long salePriceInCents;

	@Column(name = "discount_value")
	private Long discountInCents;

	@Column(name = "discount_percent")
	private Long discountPercent;

}

package com.exercise.moIpStore.domains;

public enum StreetSuffix {
	AVENUE, BOULEVARD, BRIDGE, PARK, SQUARE, STREET, VIADUCT;
}

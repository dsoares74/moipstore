package com.exercise.moIpStore.domains;

import static javax.persistence.EnumType.STRING;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_address")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class Address extends BaseEntity {

	private static final long serialVersionUID = 1559414571926727841L;

	@Id
	@GenericGenerator(
	        name = "IdAddressGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "address_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdAddressGen")
	private Long id;
	
	@Enumerated(STRING)
	private StreetSuffix streetSuffix;

	private String street;

	private String number;

	private String district;

	private String addressLine2;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private ZipCode zip;

	@Enumerated(STRING)
	private AddressType type;
}

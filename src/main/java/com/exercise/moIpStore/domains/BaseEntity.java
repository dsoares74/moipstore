package com.exercise.moIpStore.domains;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.data.annotation.LastModifiedDate;

import lombok.AccessLevel;
import lombok.Setter;

public abstract class BaseEntity implements Serializable {

	@Setter(AccessLevel.PRIVATE)
	@LastModifiedDate
	private LocalDateTime lastUpdated;

}

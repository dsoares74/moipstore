package com.exercise.moIpStore.domains;

import static javax.persistence.EnumType.STRING;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_payment")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class Payment extends BaseEntity {

	private static final long serialVersionUID = 27899403902157245L;

	@Id
	@GenericGenerator(
	        name = "IdPaymentGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "payment_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdPaymentGen")
	private Long id;
	
	@Enumerated(STRING)
	private PaymentType type;

	@Column(name = "gift_code")
	private String giftCode;

	private Long valueInCents;

	@ManyToOne 
	private BillingSlip billingSlip;

	@ManyToOne 
	private CreditCard creditCard;

	private Integer installmentsNumber;

	private Long installmentValueInCents;
}

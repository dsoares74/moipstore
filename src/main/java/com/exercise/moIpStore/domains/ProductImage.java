package com.exercise.moIpStore.domains;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_product_image")
@EqualsAndHashCode(of = { "id" })
public class ProductImage implements Serializable {

	private static final long serialVersionUID = 1404945919656616513L;

	@Id
	@GenericGenerator(
	        name = "IdProductImageGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "product_image_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdProductImageGen")
	private Long id;

	private String url;

	private Integer sortOrder;

	@ManyToOne
	private Product product;
}

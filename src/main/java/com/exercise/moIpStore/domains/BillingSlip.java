package com.exercise.moIpStore.domains;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_billing_slip")
@EqualsAndHashCode(of = { "barCode" })
public class BillingSlip implements Serializable {

	private static final long serialVersionUID = -969322699606382659L;

	@Id
	@GenericGenerator(
	        name = "IdBillingSlipGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "billingslip_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdBillingSlipGen")
	private Long id;

	private String barCode;

	private String number;

	private LocalDate dueDate;

	private String bankCode;
}

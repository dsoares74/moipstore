package com.exercise.moIpStore.domains;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_cart")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class Cart extends BaseEntity {

	private static final long serialVersionUID = 6421436593705914335L;

	@Id
	@GenericGenerator(
	        name = "IdCardGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "card_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdCardGen")
	private Long id;

	@ManyToOne
	private Customer customer;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "id.cart", cascade=CascadeType.ALL)
	private Set<CartItem> items = new HashSet<>(0);

}

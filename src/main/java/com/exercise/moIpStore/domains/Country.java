package com.exercise.moIpStore.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_country")
@EqualsAndHashCode(of = { "code" }, callSuper = false)
public class Country extends BaseEntity {

	private static final long serialVersionUID = -2019730521393801687L;

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 2)
	private String code;

	@Column(name = "name", unique = true, nullable = false, length = 35)
	private String name;
}

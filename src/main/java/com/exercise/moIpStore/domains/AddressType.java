package com.exercise.moIpStore.domains;

public enum AddressType {
	HOME, MAILING, BUSINESS, BILLING, OTHER;
}

package com.exercise.moIpStore.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_state_province")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class StateOrProvince extends BaseEntity {

	private static final long serialVersionUID = 3971365073656268474L;

	@Id
	@GenericGenerator(
	        name = "IdStateProvinceGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "state_province_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdStateProvinceGen")
	private Long id;
	
	@Column(name = "acronym", nullable = false, length = 3)
	private String acronym;
	
	@Column(name = "name", nullable = false, length = 35)
	private String name;
}

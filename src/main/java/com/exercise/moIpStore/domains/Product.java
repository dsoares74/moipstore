package com.exercise.moIpStore.domains;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_product")
@EqualsAndHashCode(of = { "code" }, callSuper = false)
public class Product extends BaseEntity {

	private static final long serialVersionUID = -4211688789267145707L;

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 15)
	private String code;

	private String name;

	private String description;

	@ManyToOne
	private Brand brand;

	@ManyToMany
    @JoinTable(name = "tb_category_product", joinColumns = @JoinColumn(name = "category_code", referencedColumnName = "code"), 
     inverseJoinColumns = @JoinColumn(name = "product_code", referencedColumnName = "code"))	
	private Collection<Category> categories = new HashSet<>();

	@OneToMany
	@JoinColumn(name = "product_code")
	private Collection<ProductImage> images = new HashSet<>();

	@Column(name = "value")
	private Long valueInCents;
}

package com.exercise.moIpStore.domains;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_ordered_product")
@EqualsAndHashCode(of = { "id" })
@AssociationOverrides({
		@AssociationOverride(name = "id.order",
			joinColumns = @JoinColumn(name = "order_id")),
		@AssociationOverride(name = "id.product",
			joinColumns = @JoinColumn(name = "product_id")) })
public class OrderedProduct implements Serializable {

	private static final long serialVersionUID = -8302068408100068525L;

	@EmbeddedId
	private OrderedProductId id;

	@Column(name = "quantity", nullable = false)
	private Integer quantity;

	@Embedded 
	private Price price;

}

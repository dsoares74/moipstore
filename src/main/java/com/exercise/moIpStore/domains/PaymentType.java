package com.exercise.moIpStore.domains;

public enum PaymentType {
	BILLING_SLIP, CREDIT_CARD;
}

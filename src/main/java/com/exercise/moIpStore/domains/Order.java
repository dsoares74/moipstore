package com.exercise.moIpStore.domains;

import static javax.persistence.EnumType.STRING;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_order")
@EqualsAndHashCode(of = { "id" }, callSuper = false)
public class Order extends BaseEntity {

	private static final long serialVersionUID = -3096122020842017404L;

	@Id
	@GenericGenerator(
	        name = "IdOrderGen",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "order_seq"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdOrderGen")
	private Long id;

	@Column(name = "sale_date")
	private LocalDateTime saleDate;

	@Column(name = "order_amount")
	private Long orderAmountInCents;

	@Column(name = "sub_total")
	private Long subTotalInCents;

	@OneToMany
	@JoinColumn(name = "id_order")
	private Collection<Payment> payments = new HashSet<>();

	@Column(name = "freight")
	private Long freightValueInCents;

	@Enumerated(STRING)
	private OrderStatus status;

	@Column(name = "delivery_name")
	private String deliveryName;

	@ManyToOne
	private Address deliveryAddress;

	@ManyToOne
	private Address billingAddress;

	@ManyToOne
	private Customer customer;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id.order", cascade=CascadeType.ALL)
	private Set<OrderedProduct> products = new HashSet<>(0);

	public Long getTotalByPaymentType(final PaymentType type) {
		return calculateTotal(payments.stream().filter(p -> p.getType() == type));
	}

	private Long calculateTotal(final Stream<Payment> streamPayment) {
		return streamPayment.map(Payment::getValueInCents).reduce(0L, Long::sum);
	}
}

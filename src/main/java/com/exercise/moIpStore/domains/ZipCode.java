package com.exercise.moIpStore.domains;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "tb_zipcode")
@EqualsAndHashCode(of = { "code" }, callSuper = false)
public class ZipCode extends BaseEntity {

	private static final long serialVersionUID = 1135569787925720814L;

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 9)
	private String code;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private City city;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private StateOrProvince stateOrProvince;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Country country;
}

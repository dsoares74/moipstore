package com.exercise.moIpStore.crypt;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CryptoConverter implements AttributeConverter<String, String> {

	@Override
	public String convertToDatabaseColumn(final String attribute) {
		return AESEncrypter.encrypt(attribute);
	}

	@Override
	public String convertToEntityAttribute(final String dbData) {
		return AESEncrypter.decrypt(dbData);
	}

}

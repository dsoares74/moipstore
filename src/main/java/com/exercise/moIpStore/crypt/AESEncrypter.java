package com.exercise.moIpStore.crypt;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;

import com.exercise.moIpStore.exception.InvalidEncryptException;

@Slf4j
class AESEncrypter {

	private static final String IV_STRING = "6a10a8a1885d3cf0";
	private static final String KEY_STRING = "ef713804927a3cf7680e0d88e14fa728";
	private static final String CIPHER_PARAMS = "AES/CBC/NoPadding";
	private static final String ALGORITHM = "AES";
	private static IvParameterSpec ivSpec;
	private static SecretKeySpec secretKeySpec;
	private static final String MESSAGE = "Text can not be empty.";

	private AESEncrypter() {
		super();
	}

	static {
		ivSpec = new IvParameterSpec(IV_STRING.getBytes());
		final byte[] key = hexStringToByteArray(KEY_STRING);
		secretKeySpec = new SecretKeySpec(key, ALGORITHM);
	}

	public static String encrypt(final String clearText) {
		if (StringUtils.isBlank(clearText)) {
			throw new IllegalArgumentException(MESSAGE);
		}

		try {
			final Cipher aesCipher = Cipher.getInstance(CIPHER_PARAMS);
			aesCipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec);

			final byte[] byteDataToEncrypt = StringUtils.rightPad(clearText, 16).getBytes();
			final byte[] byteCipherText = aesCipher.doFinal(byteDataToEncrypt);
			final Base64.Encoder encoder = Base64.getEncoder();
			final String encrypted = encoder.encodeToString(encoder.encodeToString(byteCipherText).getBytes());
			log.debug("crypting " + clearText + " => " + encrypted);
			return encrypted;
		} catch (final Exception e) {
			throw new InvalidEncryptException(e);
		}
	}

	public static String decrypt(final String encryptedText) {
		if (StringUtils.isBlank(encryptedText)) {
			throw new IllegalArgumentException(MESSAGE);
		}

		try {
			final Cipher aesCipher = Cipher.getInstance(CIPHER_PARAMS);
			aesCipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec);
			final Base64.Decoder decoder = Base64.getDecoder();
			final byte[] byteCipherText = decoder.decode(new String(decoder.decode(encryptedText)));
			final byte[] byteDecryptedText = aesCipher.doFinal(byteCipherText);
			String decrypted = new String(byteDecryptedText);

			if (StringUtils.isNotEmpty(decrypted)) {
				decrypted = decrypted.trim();
			}
			log.debug("decrypting " + encryptedText + " => " + decrypted);
			return decrypted;
		} catch (final Exception e) {
			throw new InvalidEncryptException(e);
		}
	}

	private static byte[] hexStringToByteArray(final String s) {
		final byte[] r = new byte[s.length() / 2];
		for (int i = 0; i < s.length(); i += 2) {
			r[i / 2] = (byte) Integer.parseInt(s.substring(i, 2 + i), 16);
		}
		return r;
	}
}

package com.exercise.moIpStore.exception;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

public class InvalidEncryptException extends RuntimeException {

	private static final long serialVersionUID = 2983151902869064153L;

	private static final String INVALID_ENCRYPTED = "Invalid encrypted URI.";

	public InvalidEncryptException() {
		super(INVALID_ENCRYPTED);
	}

	public InvalidEncryptException(final String message) {
		super(defaultIfBlank(message, INVALID_ENCRYPTED));
	}

	public InvalidEncryptException(final Throwable cause) {
		super(INVALID_ENCRYPTED, cause);
	}

	public InvalidEncryptException(final String message, final Throwable cause) {
		super(defaultIfBlank(message, INVALID_ENCRYPTED), cause);
	}
}

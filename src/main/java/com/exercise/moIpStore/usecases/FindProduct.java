package com.exercise.moIpStore.usecases;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exercise.moIpStore.domains.Product;
import com.exercise.moIpStore.gateway.repository.ProductRepository;

@Service
public class FindProduct {

	private final ProductRepository repository;
	
	@Autowired
	public FindProduct(final ProductRepository repository) {
		this.repository = repository;
	}
	
	public Collection<Product> findByBrand(String brand) {
		return repository.findByBrandName(brand);
	}
}
